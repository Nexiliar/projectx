// Fill out your copyright notice in the Description page of Project Settings.


#include "ProjectX/Character/ProjectXHealthComponent.h"

// Sets default values for this component's properties
UProjectXHealthComponent::UProjectXHealthComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UProjectXHealthComponent::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}


// Called every frame
void UProjectXHealthComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

float UProjectXHealthComponent::GetCurrentHealth()
{
	return Health;
}

void UProjectXHealthComponent::SetCurrentHealth(float NewHealth)
{
	Health = NewHealth;
}

void UProjectXHealthComponent::ChangeHealthValue(float ChangeValue)
{
	ChangeValue = ChangeValue * CoefDamage;
	Health = Health + ChangeValue;
	
	
	if (Health > 100.0f)
	{
		Health = 100.0f;
	}
	else
	{
		if (Health <= 0.0f)
		{
			OnDead.Broadcast();

		}
	}
	OnHealthChange.Broadcast(Health, ChangeValue);
}



